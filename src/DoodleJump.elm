port module DoodleJump exposing (main)

import Color exposing (Color)
import Canvas.Internal as Canvas

import Html exposing (Html)
import Html.Attributes as Attr
import Html.Events as Attr
import Html.Events.Extra.Touch as Touch
import Browser
import Browser.Dom as Dom
import Browser.Events as Events

import Json.Decode as Decode
import Json.Encode as Encode

import Time
import Task
import List.Extra as List
import Random
import Random.Extra as Random


--------------------------------------------
-- DoodleJump Game stuff
--------------------------------------------


type alias Model =
-- start editable modelType
    { x : Float
    , y : Float
    , xSpeed : Float
    , ySpeed : Float
    }
-- end editable modelType


main =
    gameApp game


-- start editable game
game =
    { init = init_
    , renderModel = renderCharacter
    , renderLevel = renderLevel_
    , simulate = simulate_
    , onInput = onInput_
    }


-- Womit/Wie fängt das Spiel an?
init_ : Model
init_ =
    { x = 30
    , y = 70
    , xSpeed = 0
    , ySpeed = 0
    }


nextLevel = ""


-- Was passiert, wenn der Spieler eine Taste drückt (KeyDown) oder loslässt (KeyUp)?
onInput_ input model =
    case input of
        KeyDown "ArrowLeft" ->
            { model | xSpeed = -2 }

        KeyDown "ArrowRight" ->
            { model | xSpeed = 2 }

        KeyUp "ArrowLeft" ->
            { model | xSpeed = 0 }

        KeyUp "ArrowRight" ->
            { model | xSpeed = 0 }

        _ ->
            model


-- Zeichnet die Spielfigur
renderCharacter model =
    image characterImage
        |> move (model.x, model.y)



-- Positionen und Größen der Plattformen
platforms : List Platform
platforms =
    [ { height = 30, middle = 45, width = 36 }
    , { height = 50, middle = 60, width = 24 }
    , { height = 70, middle = 80, width = 24 }
    , { height = 90, middle = 110, width = 48 }
    , { height = 110, middle = 135, width = 12 }
    , { height = 160, middle = 110, width = 48 }
    ]


-- Position des Diamanten
goalItemPosition = (110, 170)




-- Berechnet, wie sich das Spiel in einem Zeitschritt verändert
simulate_ : Model -> Model
simulate_ model =
    model
        |> physics
        |> gravity
        |> bounce


physics model =
    { model
        | x = model.x + model.xSpeed
        , y = model.y + model.ySpeed
        }


gravity model =
    { model | ySpeed = model.ySpeed - 0.2 }





jumpStrength = 4

-- Springe vom Boden oder einer Plattform wieder ab
bounce model =
    if model.y <= 0 then
        { model | ySpeed = jumpStrength }
    else if anyCollision model platforms then
        { model | ySpeed = jumpStrength }
    else
        model


-- Springe links und rechts vom Rand ab
bounceWalls model =
    if model.x >= gameWidth - charWidth / 2 then
        { model | xSpeed = -(abs model.xSpeed) }
    else if model.x <= charWidth / 2 then
        { model | xSpeed = abs model.xSpeed }
    else
        model




renderLevel_ =
    fillBackground (Color.hsl 0.53 0.5 0.85)
        |> atop renderGround
        |> atop renderPlatforms


fillBackground color =
    fillRect gameWidth gameHeight color


renderGround =
    pattern groundPattern gameWidth groundSize



-- Die unterschiedlichen Bilder, die wir benutzen

characterImage =
    { source = tileSource "Characters" "platformChar_jump"
    , width = charWidth
    , height = charWidth
    }


goalItemImage =
    { source = tileSource "Items" "platformPack_item010"
    , width = charWidth
    , height = charWidth
    }


platformPattern =
    { source = tileSource "Tiles" "platformPack_tile025"
    , repeat = "repeat-x"
    , scalePattern = 0.25
    }


groundPattern =
    { source = tileSource "Tiles" "platformPack_tile001"
    , repeat = "repeat-x"
    , scalePattern = 0.25
    }



-- Hilfsfunktionen

signum num =
    if num > 0 then
        1
    else if num < 0 then
        -1
    else
        num


within bottom top value =
    bottom <= value && value <= top


around middle w value =
    abs (value - middle) <= w / 2




-- Für Fortgeschrittene

type alias Platform =
    { height : Float
    , middle : Float
    , width : Float
    }


renderPlatforms =
    let
        renderPlatform platform =
            pattern platformPattern platform.width 12
                |> move (platform.middle - platform.width / 2, platform.height)
    in
    List.map renderPlatform platforms
        |> atopAll


-- Breite Charakterbilds
charWidth = 20

-- Größe des Rands auf jeder Seite des Charakterbilds, mit der er _nicht_ stehen kann
--   Also sind links und rechts vom Bild jeweils 4 Pixel, die nicht durch die Füße
--   des Character abgedeckt sind. Wenn das Bild also mit diesem Teil noch über einer
--   Plattform wäre, soll er trotzdem durchfallen.
--   Ändere zum Ausprobieren mal `image` in der `renderCharacter`-Funktion zu `debugImage`
--   Um zu sehen wie und wo es benutzt wird, schaue in die `collides`-Funktion
charPadding = 4


-- Kommt der Spieler auf einer Plattform auf?
collides model platform =
    around platform.middle (platform.width + charWidth - 2 * charPadding) model.x
    && within (platform.height + model.ySpeed) platform.height model.y


anyCollision model platformsToTest =
    List.any (collides model) platformsToTest


-- end editable game








----------------------------------------------------------------------
-- Hidden Game Elements
----------------------------------------------------------------------



gameWidth =
    200


gameHeight =
    200


groundSize =
    16


gameViewScale =
-- start editable scale
    0
-- end editable scale


scaleToWindow (winWidth, winHeight) =
    if abs gameViewScale < 0.1 then
        min (winWidth / gameWidth) (winHeight / (gameHeight + groundSize))
    else
        gameViewScale


view_ : GameApp Model -> HiddenState Model -> Html (Msg Model)
view_ ({ renderModel, renderLevel, simulate } as gameDesc) model =
    let
        scaleFactor =
            scaleToWindow model.windowSize
    in
    [ Maybe.map renderParticles model.particles
        |> Maybe.withDefault empty
    , viewGoalItem model.particles
    , viewDynamic gameDesc simulate model
    , renderLevel
    ]
        |> atopAll
        |> translate 0 gameHeight
        |> scale scaleFactor scaleFactor
        |> toHtml
            (scaleFactor * gameWidth)
            (scaleFactor * (gameHeight + groundSize))
            [ Touch.onStart (Input << Taps << List.map (scaleVec (1/scaleFactor) << .clientPos) << .targetTouches)
            , Touch.onMove (Input << Taps << List.map (scaleVec (1/scaleFactor) << .clientPos) << .targetTouches)
            , Touch.onEnd (Input << Taps << List.map (scaleVec (1/scaleFactor) << .clientPos) << .targetTouches)
            ]


viewDynamic gameDesc simulate model =
    if model.paused && model.historyPosition /= 0 then
        let
            modelStates =
                expandHistory gameDesc model.history

            modelAtStep =
                List.getAt model.historyPosition modelStates

            modelsAfter =
                List.take model.historyPosition modelStates
        in
        case modelAtStep of
            Just userModel ->
                (if model.forwardHistoryVisible then modelsAfter else [])
                    |> List.reverse
                    |> List.map (\{ start, msg } -> updateUserModel gameDesc msg start)
                    |> List.indexedMap (\index -> gameDesc.renderModel >> withOpacitySetTo (0.6 * 0.975 ^ toFloat index))
                    |> atopAll
                    |> atop (gameDesc.renderModel (updateUserModel gameDesc userModel.msg userModel.start))

            Nothing ->
                gameDesc.renderModel model.userState
    else
        empty
            |> atop (renderTrail model.trail 0.6 gameDesc.renderModel simulate (simulate model.userState))
            |> atop (gameDesc.renderModel model.userState)


viewGoalItem particles =
    case particles of
        Just _ ->
            empty

        Nothing ->
            image goalItemImage
                |> move goalItemPosition


tileSource typ name =
    "/assets/kenney_simplifiedplatformer/PNG/" ++ typ ++ "/" ++ name ++ ".png"


renderTrail n opacity renderModel simulate model =
    if n > 0 then
        renderTrail (n - 1) (0.975 * opacity) renderModel simulate (simulate model)
            |> atop (renderModel model)
            |> withOpacitySetTo opacity
    else
        empty















--------------------------------------------
-- Graphics Abstraction Stuff
--------------------------------------------


type alias Graphics =
    ( Bool, List Canvas.Command )


toHtml width height attrs commands =
    Html.node "elm-canvas"
        [ Canvas.commands (withSave (clear (0, 0) width height commands))
        , Canvas.size width height
        ]
        [ Html.canvas attrs []
        ]


clear (x, y) width height ( shouldSave, commands ) =
    [ Canvas.clearRect x y width height ]
        |> (++) commands
        |> Tuple.pair shouldSave


empty =
    []
        |> Tuple.pair False


translate x y ( _, cmds ) =
    []
        |> (::) (Canvas.translate x y)
        |> (++) cmds
        |> Tuple.pair True


move (x, y) =
    translate x -y


image { source, width, height } =
    [ Canvas.drawImage source (-width / 2) -height width height ]
        |> Tuple.pair False


debugImage { source, width, height } =
    [ Canvas.fill Canvas.NonZero
    , Canvas.circle 0 0 1
    , Canvas.beginPath
    , Canvas.fillStyle Color.red
    , Canvas.strokeRect (-width / 2) -height width height
    , Canvas.drawImage source (-width / 2) -height width height
    ]
        |> Tuple.pair False


pattern { source, repeat, scalePattern } width height =
    []
        |> (::) (Canvas.scale scalePattern scalePattern)
        |> (::) (Canvas.fillPattern source repeat)
        |> (::) (Canvas.fillRect 0 0 (width / scalePattern) (height / scalePattern))
        |> Tuple.pair True


fillRect width height color =
    []
        |> (::) (Canvas.fillStyle color)
        |> (::) (Canvas.fillRect 0 -height width height)
        |> Tuple.pair False


scale x y ( _, cmds ) =
    []
        |> (::) (Canvas.scale x y)
        |> (++) cmds
        |> Tuple.pair True


rotate angle ( _, cmds ) =
    []
        |> (::) (Canvas.rotate angle)
        |> (++) cmds
        |> Tuple.pair True


atop ( shouldSave, top ) bottom =
    top ++ withSave bottom
        |> Tuple.pair shouldSave


atopAll =
    List.foldr atop empty


withSave ( shouldSave, commands ) =
    if shouldSave then
        []
            |> (::) Canvas.save
            |> (++) commands
            |> (::) Canvas.restore
    else
        commands


withOpacitySetTo opacity ( _, commands ) =
    []
        |> (::) (Canvas.globalAlpha opacity)
        |> (++) commands
        |> Tuple.pair True















-----------------------------------------------------
-- Game Engine
-----------------------------------------------------


type Msg model
    = Step
    | PlayPause
    | Restart
    | Noop
    | ChangeModel model
    | Input Input
    | CodeViewMsg CodeViewMsg
    | SelectEvent Int
    | ToggleEventInspector
    | ChangeTrail Int
    | ChangeHistoryPosition Int
    | ChangeForwardHistoryVisible Bool
    | NextLevel String
    | Resize Float Float


type Input
    = KeyDown String
    | KeyUp String
    | Taps (List (Float, Float))


type alias HiddenState model  =
    { userState : model
    , paused : Bool
    , windowSize : ( Float, Float )
    , trail : Int
    , particles : Maybe Particles
    , history : List { start : model, msg : Msg model, count : Int }
    , historyPosition : Int
    , forwardHistoryVisible : Bool
    , lastInputEvent : Input
    , codeView : CodeView
    , eventInspector : { visible : Bool, selectedEvent : Int, events : List Input }
    }


type alias GameApp model =
    { init : model
    , renderModel : model -> Graphics
    , renderLevel : Graphics
    , simulate : model -> model
    , onInput : Input -> model -> model
    }


type alias GameProgram model =
    Program () (HiddenState model) (Msg model)


initHiddenState : model -> () -> ( HiddenState model, Cmd (Msg model) )
initHiddenState initUserState () =
    ( { userState = initUserState
      , paused = False
      , windowSize = ( 0, 0 )
      , trail = 0
      , particles = Nothing
      , history = []
      , historyPosition = 0
      , forwardHistoryVisible = False
      , lastInputEvent = Taps []
      , codeView = codeViewInit
      , eventInspector =
        { visible = False
        , selectedEvent = 0
        , events = []
        }
      }
    , Task.perform (\v -> Resize v.viewport.width v.viewport.height) Dom.getViewport
    )


noInteraction : Graphics -> GameApp {}
noInteraction render =
    { init = {}
    , renderModel = \{} -> empty
    , renderLevel = render
    , simulate = \{} -> {}
    , onInput = \input {} -> {}
    }


gameApp : GameApp Model -> GameProgram Model
gameApp gameDesc =
    Browser.element
        { init = initHiddenState gameDesc.init
        , view = viewGameApp gameDesc
        , update =
            ( \msg model ->
                model
                    |> logMsg msg
                    |> updateGameApp gameDesc msg
            )
                |> fixInputRepeat
        , subscriptions =
            \model ->
                Sub.batch
                    [ if model.paused then Sub.none else Events.onAnimationFrameDelta (always Step)
                    , Events.onKeyDown (Decode.map (Input << KeyDown) keyDecoder)
                    , Events.onKeyUp (Decode.map (Input << KeyUp) keyDecoder)
                    , Events.onResize (\w h -> Resize (toFloat w) (toFloat h))
                    ]
        }


fixInputRepeat : (Msg Model -> HiddenState Model -> ( HiddenState Model, Cmd (Msg Model) )) -> Msg Model -> HiddenState Model -> ( HiddenState Model, Cmd (Msg Model) )
fixInputRepeat update msg model =
    case msg of
        Input input ->
            if input == model.lastInputEvent then
                ( model, Cmd.none )
            else
                update msg { model | lastInputEvent = input }

        _ ->
            update msg model


viewGameApp : GameApp Model -> HiddenState Model -> Html (Msg Model)
viewGameApp =
-- start editable viewGameApp
    viewGameAppInspector
-- end editable viewGameApp


viewGameControls : HiddenState model -> Html (Msg model) -> Html (Msg model)
viewGameControls model child =
    Html.div
        [ Attr.style "display" "inline-block" ]
        [ case model.particles of
            Just _ ->
                Html.div
                    -- [ Attr.style "display" "inline"
                    [ Attr.style "position" "relative"
                    ]
                    [ Html.div
                        [ Attr.style "display" "inline"
                        , Attr.style "position" "absolute"
                        , Attr.style "left" "10px"
                        , Attr.style "top" "10px"
                        ]
                        [ Html.div [ Attr.style "font-size" "30px" ] [ Html.text "Success!" ]
                        , Html.div []
                            [ Html.button [ Attr.onClick Restart ] [ Html.text "Restart" ]
                            , (if String.isEmpty nextLevel then Nothing else Just nextLevel)
                                |> Maybe.andThen (List.last << String.split "?")
                                |> Maybe.map (\query -> Html.button [ Attr.onClick (NextLevel query) ] [ Html.text "Next Level" ])
                                |> Maybe.withDefault (Html.text "")
                            ]
                        ]
                    ]

            Nothing ->
                Html.div [] []
        , child
        ]


viewGameAppGame : GameApp Model -> HiddenState Model -> Html (Msg Model)
viewGameAppGame gameDesc model =
    view_ gameDesc model
        |> viewGameControls model


viewGameAppInspector : GameApp Model -> HiddenState Model -> Html (Msg Model)
viewGameAppInspector gameDesc model =
    Html.div [ Attr.style "margin" "5px" ]
        [ view_ gameDesc model
            |> viewGameControls model
        , Html.div []
            [ Html.button [ Attr.onClick Restart ] [ Html.text "↺" ]
            , Html.button [ Attr.onClick PlayPause ] [ Html.text (if model.paused then "►" else "||") ]
            , Html.button [ Attr.onClick Step ] [ Html.text "►|" ]
            ]

        , viewParams model

        , Html.h3 [] [ Html.text "Current State" ]
        , Html.map ChangeModel <| viewModel model.userState

        -- , if model.paused then Html.h3 [] [ Html.text "Simulation Code" ] else Html.text ""
        -- , Html.map CodeViewMsg codeView

        , Html.h3 [] [ Html.text "Timeline" ]
        , viewTimeline model

        , if model.paused then viewEventInspector gameDesc.onInput model.userState model.eventInspector else Html.text ""
        ]




port loadNextLevel : String -> Cmd msg


updateGameApp ({ onInput, simulate, init } as gameDesc) msg model =
    case msg of
        NextLevel query ->
            ( model, loadNextLevel query )

        Resize w h ->
            ( { model | windowSize = (w, h) }, Cmd.none )

        Input input ->
            ( { model
                | userState = onInput input model.userState
                , eventInspector =
                    if model.paused then
                        let
                            eventInspector = model.eventInspector
                        in
                        { eventInspector
                            | events =
                                List.head model.eventInspector.events
                                    |> Maybe.map (\lastEvent -> if lastEvent == input then [] else [ input ])
                                    |> Maybe.map (\newEvent -> newEvent ++ model.eventInspector.events)
                                    |> Maybe.withDefault [ input ]
                        }
                    else
                        model.eventInspector
                }
            , Cmd.none
            )

        ChangeModel newModel ->
            ( { model | userState = newModel }, Cmd.none )

        PlayPause ->
            ( { model | paused = not model.paused, eventInspector = { visible = model.eventInspector.visible, selectedEvent = 0, events = [] } }, Cmd.none )

        Step ->
            ( stepGame gameDesc model, Cmd.none )

        Restart ->
            ( { model | userState = init, particles = Nothing, history = [], historyPosition = 0 }, Cmd.none )

        CodeViewMsg codeViewMsg ->
            ( { model | codeView = codeViewUpdate codeViewMsg model.codeView }, Cmd.none )

        SelectEvent index ->
            ( { model
                | eventInspector =
                    let
                        eventInspector = model.eventInspector
                    in
                    { eventInspector | selectedEvent = index }
                }
            , Cmd.none
            )

        ToggleEventInspector ->
            ( { model
                | eventInspector =
                    let
                        eventInspector = model.eventInspector
                    in
                    { eventInspector | visible = not eventInspector.visible }
                }
            , Cmd.none
            )

        ChangeTrail trail ->
            ( { model | trail = trail }, Cmd.none )

        ChangeHistoryPosition position ->
            ( { model | historyPosition = position }, Cmd.none )

        ChangeForwardHistoryVisible fwdHistVis ->
            ( { model | forwardHistoryVisible = fwdHistVis }, Cmd.none )

        _ ->
            ( model, Cmd.none )



stepGame : GameApp Model -> HiddenState Model -> HiddenState Model
stepGame { simulate } model =
    let
        gameState =
            simulate model.userState

        newParticles =
            case model.particles of
                Just particles ->
                    Just (stepParticles particles)

                Nothing ->
                    if distSquared (model.userState.x, model.userState.y) goalItemPosition <= (charWidth / 2) ^ 2 then
                        Just (initParticles gameState)
                    else
                        Nothing
    in
    { model
        | userState = gameState
        , particles = newParticles
        }





----------------------------------------------------------------
-- Particle System
----------------------------------------------------------------


type alias Particles =
    List Particle


type alias Particle =
    { x : Float
    , y : Float
    , xSpeed : Float
    , ySpeed : Float
    , width : Float
    , height : Float
    , rotation : Float
    , rotSpeed : Float
    , color : Color
    }


randomParticle : Random.Generator Particle
randomParticle =
    let
        makeParticle (xSpeed, ySpeed) =
            Particle
                (Tuple.first goalItemPosition)
                (Tuple.second goalItemPosition + charWidth / 2)
                xSpeed
                ySpeed
    in
    Random.map6 makeParticle
        (Random.map2 directionToVector
            (Random.float -180 180)
            (Random.float 1 10))
        (Random.float 4 7)
        (Random.float 2 3)
        (Random.float 0 360)
        (Random.float 0.1 2)
        (Random.map3 Color.hsl
            (Random.float 0 1)
            (Random.float 0.8 1)
            (Random.float 0.4 0.6))


randomParticles : Random.Generator Particles
randomParticles =
    Random.int 200 400
        |> Random.andThen (\count -> Random.list count randomParticle)


generateRandom : Int -> Random.Generator a -> a
generateRandom initialSeed generator =
    Random.step generator (Random.initialSeed initialSeed)
        |> Tuple.first


initParticles : Model -> Particles
initParticles { x, y } =
    randomParticles
        |> generateRandom (round (x * 100 + y * 99))


stepParticles particles =
    particles
        |> List.map stepParticle
        |> List.filter (\{ y } -> y >= -groundSize - 10)


stepParticle particle =
    let
        dampening = 0.96
    in
    { particle
        | x = particle.x + particle.xSpeed
        , y = particle.y + particle.ySpeed
        , xSpeed = dampening * particle.xSpeed
        , ySpeed = dampening * (particle.ySpeed - 0.2)
        , rotation = particle.rotation + particle.rotSpeed
        , rotSpeed = dampening * particle.rotSpeed
        }


renderParticles particles =
    particles
        |> List.map renderParticle
        |> atopAll


renderParticle particle =
    particle.color
        |> fillRect particle.width particle.height
        |> move (-particle.width / 2, -particle.height / 2)
        |> rotate particle.rotation
        |> move (particle.x, particle.y)







--------------------------------------------------------------
-- Timeline
--------------------------------------------------------------

updateUserModel : GameApp model -> Msg model -> model -> model
updateUserModel { simulate, onInput } msg model =
    case msg of
        Step ->
            simulate model

        Input input ->
            onInput input model

        ChangeModel newModel ->
            newModel

        _ ->
            model


expandHistory : GameApp Model -> List { start : Model, msg : Msg Model, count : Int } -> List { start : Model, msg : Msg Model }
expandHistory gameDesc history =
    let
        nextEntry { start, msg } =
            { start = updateUserModel gameDesc msg start, msg = msg }
    in
    List.concatMap
        (\{ start, msg, count } ->
            List.reverse (initerate count nextEntry { start = start, msg = msg }))
        history


expandHistoryMessages : List { a | msg : msg, count : Int } -> List msg
expandHistoryMessages history =
    List.concatMap
        (\{ msg, count } -> List.repeat count msg)
        history


collapseHistoryMessages : List msg -> List { msg : msg, count : Int }
collapseHistoryMessages history =
    List.group history
        |> List.map (\(msg, other) -> { msg = msg, count = 1 + List.length other })


collapseHistory : List { start : Model, msg : Msg Model } -> List { start : Model, msg : Msg Model, count : Int }
collapseHistory history =
    case history of
        first :: restExpanded ->
            case collapseHistory restExpanded of
                second :: restCollapsed ->
                    if first.msg == second.msg then
                        { second | count = second.count + 1 } :: restCollapsed
                    else
                        { start = first.start, msg = first.msg, count = 1 } :: second :: restCollapsed

                [] ->
                    [{ start = first.start, msg = first.msg, count = 1 }]

        [] ->
            []


logMsg msg model =
    let
        newEntry =
            case msg of
                Step ->
                    [ { start = model.userState, msg = msg, count = 1 } ]

                Input _ ->
                    [ { start = model.userState, msg = msg, count = 1 } ]

                ChangeModel _ ->
                    [ { start = model.userState, msg = msg, count = 1 } ]

                _ ->
                    []

        mergeSame list =
            case list of
                first :: second :: rest ->
                    case (first.msg, second.msg) of
                        (ChangeModel _, ChangeModel _) ->
                            first :: rest

                        _ ->
                            if first.msg == second.msg then
                                { second | count = first.count + second.count } :: rest
                            else
                                list

                _ ->
                    list
    in
    { model | history = mergeSame (newEntry ++ model.history) }


viewTimeline : HiddenState Model -> Html msg
viewTimeline model =
    ( if model.paused then
        case List.splitAt model.historyPosition (expandHistoryMessages model.history) of
            (after, current :: before) ->
                [ List.map viewTimelineEntry (collapseHistoryMessages after)
                , [ Html.div [ Attr.style "background-color" "lightgrey" ] [ viewTimelineEntry { msg = current, count = 1 } ] ]
                , List.map viewTimelineEntry (collapseHistoryMessages before)
                ]
                    |> List.concat

            (_, []) ->
                List.map viewTimelineEntry model.history
      else
        List.map viewTimelineEntry model.history
    )
        |> Html.div [ Attr.style "line-height" "1.5", Attr.style "font-family" "monospace" ]


viewTimelineEntry { msg, count } =
    Html.div []
        [ case msg of
            Step ->
                Html.text "simulate"

            Input input ->
                case input of
                    KeyDown key ->
                        Html.text ("onInput (KeyDown \"" ++ key ++ "\")")

                    KeyUp key ->
                        Html.text ("onInput (KeyUp \"" ++ key ++ "\")")

                    Taps taps ->
                        Html.text "Some Taps -- TODO" -- TODO

            ChangeModel _ ->
                Html.text "user changes"

            _ ->
                Html.text ""
        , if count > 1 then
            Html.text (String.concat [ " (", String.fromInt count, ")" ])
            else
            Html.text ""
        ]










--------------------------------------------------------------
-- Event Inspector
--------------------------------------------------------------


viewEventInspector onInput gameState { selectedEvent, events, visible } =
    let
        showEvent active index event =
            Html.div
                [ if active then Attr.style "background-color" "lightgrey" else Attr.value ""
                , Attr.style "cursor" "pointer"
                , Attr.onClick (SelectEvent index)
                ]
                [ Html.text (Debug.toString event) ]
    in
    Html.div []
        [ Html.h3 []
            [ Html.text "Input Inspector"
            , Html.span
                [ Attr.style "cursor" "pointer"
                , Attr.style "font-size" "12px"
                , Attr.style "color" "grey"
                , Attr.style "margin-left" "1em"
                , Attr.onClick ToggleEventInspector
                ]
                [ Html.text (if visible then "hide" else "show")
                ]
            ]
        , Html.div
            [ Attr.style "display" "inline-block"
            , Attr.style "line-height" "1.5"
            , Attr.style "font-family" "monospace"
            ]
            (List.indexedMap (\index event -> showEvent (index == selectedEvent) index event) events)
            |> if visible then identity else always (Html.text "")
        , List.getAt selectedEvent events
            |> Maybe.map (\event -> viewModel (onInput event gameState))
            |> Maybe.map (Html.map (always Noop))
            |> Maybe.withDefault (Html.text "No events yet")
            |> if visible then identity else always (Html.text "")
        ]







-----------------------------------------------------------
-- Model Inspection Stuff
-----------------------------------------------------------


inspectFloat value =
    inspectFloatCustom 2 (roundOn 2) value


inspectFloatCustom scale_ f value =
    let
        quickfixThreshold =
            15 / scale_

        quickfixBrowser v =
            if v < 0 && abs (v - value) > quickfixThreshold then value else v
    in
    Html.div
        [ Attr.style "display" "inline-block"
        , Attr.draggable "true"
        , Attr.on "drag" (Decode.map (\v -> quickfixBrowser (f (v / scale_))) (Decode.field "offsetX" Decode.float))
        , Attr.on "dragstart" (Decode.map (\v -> f (v / scale_)) (Decode.field "offsetX" Decode.float))
        ]
        [ Html.span
            [ Attr.style "color" "grey"
            , Attr.style "cursor" "pointer"
            , Attr.style "font-size" "8px"
            , Attr.onClick (value - 1 / scale_)
            ]
            [ Html.text "◀︎" ]
        , Html.span
            [ Attr.style "cursor" "grab"
            , Attr.style "margin" "0px 2px" ]
            [ Html.text (String.fromFloat (f value)) ]
        , Html.span
            [ Attr.style "color" "grey"
            , Attr.style "cursor" "pointer"
            , Attr.style "font-size" "8px"
            , Attr.onClick (value + 1 / scale_)
            ]
            [ Html.text "▶︎" ]

        ]


inspectIntCustom scale_ value =
    let
        quickfixThreshold =
            15 / scale_

        quickfixBrowser v =
            if v < 0 && abs (v - toFloat value) > quickfixThreshold then value else round v
    in
    Html.div
        [ Attr.style "display" "inline-block"
        , Attr.draggable "true"
        , Attr.on "drag" (Decode.map (\v -> quickfixBrowser (v / scale_)) (Decode.field "offsetX" Decode.float))
        , Attr.on "dragstart" (Decode.map (\v -> round (v / scale_)) (Decode.field "offsetX" Decode.float))
        ]
        [ Html.span
            [ Attr.style "color" "grey"
            , Attr.style "cursor" "pointer"
            , Attr.style "font-size" "8px"
            , Attr.onClick (value - 1)
            ]
            [ Html.text "◀︎" ]
        , Html.span
            [ Attr.style "cursor" "grab"
            , Attr.style "margin" "0px 2px" ]
            [ Html.text (String.fromInt value) ]
        , Html.span
            [ Attr.style "color" "grey"
            , Attr.style "cursor" "pointer"
            , Attr.style "font-size" "8px"
            , Attr.onClick (value + 1)
            ]
            [ Html.text "▶︎" ]

        ]


inspectBool value =
    Html.div
        [ Attr.style "display" "inline-block"
        , Attr.style "cursor" "pointer"
        , Attr.onClick (not value)
        ]
        [ Html.text (if value then "True" else "False") ]


inspectLabel name typ value =
    Html.div []
        [ Html.span [ Attr.style "color" "darkgreen" ] [ Html.text name ]
        , Html.span [] [Html.text " = "]
        , typ value
        ]


inspectVerticalRecord (fields, value) =
    Html.div []
        [ Html.text "{"
        , Html.div [ Attr.style "margin-left" "2em" ] fields
        , Html.text "}"
        ]


inspectConstructor constr =
    ([], constr)


inspectWithField typ value (fields, constr) =
    let
        fieldsWithValue =
            List.map (Html.map (\constrBefore -> constrBefore value)) fields
    in
    ( fieldsWithValue ++ [ Html.map constr (typ value) ]
    , constr value
    )


inspectWithEntry name typ =
    inspectWithField (inspectLabel name typ)


inspectHidden _ _ _ value =
    inspectHiddenField value


inspectHiddenField value (fields, constr) =
    ( List.map (Html.map (\constrBefore -> constrBefore value)) fields
    , constr value
    )


inspectMap =
    Html.map


viewInspector inspector =
    Html.div [ Attr.style "font-family" "monospace" ]
        [ inspector ]



viewParams model =
    Html.div []
        [ viewTrail model.trail
            |> Html.map ChangeTrail
        , if model.paused then
            inspectLabel "pastPosition" (inspectIntCustom 2) model.historyPosition
                |> viewInspector
                |> Html.map (ChangeHistoryPosition << \pos -> max 0 pos)
          else
            Html.text ""
        , if model.paused then
            inspectLabel "viewFuture" inspectBool model.forwardHistoryVisible
                |> viewInspector
                |> Html.map ChangeForwardHistoryVisible
          else
            Html.text ""
        ]

viewTrail trail =
    inspectLabel "trail" (inspectIntCustom 50) trail
        |> viewInspector


viewModel model =
    inspectConstructor Model
-- start editable modelInspector
        |> inspectWithEntry "x" inspectFloat model.x
        |> inspectWithEntry "y" inspectFloat model.y
        |> inspectWithEntry "xSpeed" inspectFloat model.xSpeed
        |> inspectWithEntry "ySpeed" inspectFloat model.ySpeed
-- end editable modelInspector
        |> inspectVerticalRecord
        |> viewInspector
















-----------------------------------------------------------------
-- Old Running Code Viewing Stuff
-----------------------------------------------------------------


type alias CodeView =
    List
        { expanded: Bool
        , showStep: Bool
        , active: Bool
        }


type CodeViewMsg
    = CVNoop
    | PipelineStep Int StepMsg

type StepMsg
    = ExpandCollapse
    | ViewHide
    | ToggleActive
    | StepNoop


codeViewInit =
    [ { expanded = False
      , showStep = False
      , active = True
      }
    , { expanded = False
      , showStep = False
      , active = True
      }
    , { expanded = False
      , showStep = False
      , active = True
      }
    , { expanded = False
      , showStep = False
      , active = True
      }
    , { expanded = False
      , showStep = False
      , active = True
      }
    ]


codeViewUpdate msg model =
    case msg of
        CVNoop ->
            model

        PipelineStep index stepMsg ->
            List.updateAt index (updateStep stepMsg) model


updateStep msg step =
    case msg of
        ExpandCollapse ->
            { step | expanded = not step.expanded }

        ViewHide ->
            { step | showStep = not step.showStep }

        ToggleActive ->
            { step | active = not step.active }

        StepNoop ->
            step


viewSimulate codeView model =
    let
        indented =
            Attr.style "margin-left" "2em"

        viewStep funcName modelAfter stepModel =
            let
                viewStepHtml =
                    if stepModel.active then
                        Html.div []
                            [ Html.span
                                [ Attr.onClick ToggleActive
                                , Attr.style "cursor" "pointer"
                                ]
                                [ Html.text "|> "
                                , Html.text funcName
                                ]
                            , Html.span
                                [ Attr.onClick ExpandCollapse
                                , Attr.style "cursor" "pointer"
                                ]
                                [ Html.text <| if stepModel.expanded then " ▾ " else " ▸ " ]
                            , Html.span
                                [ Attr.onClick ViewHide
                                , Attr.style "cursor" "pointer"
                                , if stepModel.showStep then
                                    Attr.style "color" "black"
                                    else
                                    Attr.style "color" "grey"
                                ]
                                [ Html.text " ⦿ " ]
                            , if stepModel.expanded then
                                Html.div [ indented ]
                                    [ Html.map (always StepNoop) (viewModel modelAfter) ]
                                else
                                Html.text ""
                            ]
                    else
                        Html.div
                            [ Attr.style "color" "grey"
                            , Attr.onClick ToggleActive
                            , Attr.style "cursor" "pointer"
                            ]
                            [ Html.text "|> "
                            , Html.text funcName
                            ]

                modelToShow =
                    if stepModel.showStep then
                        [ modelAfter ]
                    else
                        []
            in
            ( viewStepHtml, modelToShow )

        ( pipeline, modelsToShow ) =
            -- [ ( "physMove", physMove )
            -- , ( "gravity", gravity )
            -- , ( "bounce", bounce )
            -- , ( "bounceWalls", bounceWalls )
            -- , ( "userMove", userMove )
            [
            ]
                |> List.unzip
                |> \(names, funcs) ->
                    List.map3 (\name modelAfter stepModel -> (name, modelAfter, stepModel))
                        names
                        (List.drop 1 <| List.scanl (<|) model funcs)
                        codeView
                |> List.indexedMap
                    (\index (funcName, modelAfter, stepModel) ->
                        let
                            ( html, models ) =
                                viewStep funcName modelAfter stepModel
                        in
                        ( Html.map (PipelineStep index) html, models )
                    )
                |> List.unzip

        code =
            Html.div [ Attr.style "font-family" "monospace" ]
                [ Html.text "simulate model ="
                , Html.div [ indented ]
                    [ Html.text "model"
                    , Html.div [ indented ] pipeline
                    ]
                ]

    in
    ( code, List.concat modelsToShow )








viewSimulation model =
    let
        block attrs =
            Html.div (Attr.style "display" "inline-block" :: attrs)

        blockStyle =
            [ Attr.style "background-color" "rgba(100, 100, 100, 0.1)"
            , Attr.style "padding" "2px"
            ]

        keyword name =
            Html.span [ Attr.style "color" "darkMagenta" ] [ Html.text name ]

        horiz attrs bot top =
            block attrs
                [ Html.div
                    [ Attr.style "width" "100%"
                    , Attr.style "text-align" "center"
                    ]
                    top
                , Html.div
                    [ Attr.style "width" "100%"
                    , Attr.style "text-align" "center"
                    , Attr.style "margin-top" "2px"
                    ]
                    bot
                ]

        viewRecordUpdate fieldName fieldValue =
            Html.div
                []
                [ Html.text "{ model | "
                , Html.text fieldName
                , Html.text " = "
                , fieldValue
                , Html.text " }"
                ]

        viewRecordUpdates var fields =
            let
                viewRecordFieldUpdate (fieldName, fieldValue) =
                    Html.div
                        [ Attr.style "margin-top" "1em" ]
                        [ Html.text fieldName
                        , Html.text " = "
                        , Html.div [] [ block [ Attr.style "margin-left" "2em" ] [ fieldValue ] ]
                        ]
            in
            block
                blockStyle
                [ Html.text "{ "
                , Html.text var
                , Html.text " |"
                , Html.div
                    [ Attr.style "margin-left" "2em" ]
                    (List.map viewRecordFieldUpdate fields)
                , Html.text "}"
                ]

        viewSimulationStep funcName body result =
            Html.div
                [ Attr.style "margin-top" "2em" ]
                [ Html.text funcName
                , Html.text " model ="
                , Html.div
                    [ Attr.style "margin-left" "2em"
                    , Attr.style "margin-bottom" "1em"
                    ]
                    [ body ]
                , Html.map (always Noop) <| viewModel result
                ]

        viewInfix viewResult op leftArg rightArg =
            { view =
                horiz blockStyle
                    [ Html.span [ Attr.style "color" "darkBlue" ] [ viewResult (op.value leftArg.value rightArg.value) ] ]
                    [ leftArg.view
                    , op.view
                    , rightArg.view
                    ]
            , value =
                op.value leftArg.value rightArg.value
            }

        viewIf cond trueBranch elseBranch =
            let
                viewBranch active branch =
                    Html.div
                        [ Attr.style "margin-left" "2em"
                        , Attr.style "margin-top" "1em"
                        , if active then Attr.style "border" "1px solid black" else Attr.value ""
                        ]
                        [ branch.view ]
            in
            { view =
                block
                    blockStyle
                    [ Html.div [] [ keyword "if ", cond.view, keyword " then" ]
                    , viewBranch cond.value trueBranch
                    , Html.div [] [ keyword "else" ]
                    , viewBranch (not cond.value) elseBranch
                    ]
            , value =
                if cond.value then trueBranch.value else elseBranch.value
            }

        viewFloatValue value =
            Html.span [ Attr.style "color" "darkBlue" ] [ Html.text (fromFloat value) ]

        viewBoolValue value =
            Html.span [ Attr.style "color" "darkBlue" ] [ Html.text (if value then "True" else "False") ]

        viewConstant viewValue text value =
            { value = value
            , view =
                horiz blockStyle
                    [ viewValue value ]
                    [ Html.text text ]
            }

        plus = { view = Html.text " + ", value = (+) }
        minus = { view = Html.text " - ", value = (-) }
        lteq = { view = Html.text " <= ", value = (<=) }
        modelY = viewConstant viewFloatValue "model.y" model.y
        modelSpeed = viewConstant viewFloatValue "model.speed" model.speed
        viewFloat value = viewConstant viewFloatValue (String.fromFloat value) value
        viewBool value = viewConstant viewBoolValue (if value then "True" else "False") value

        viewSimulationAll =
            let
                yValue =
                    viewInfix viewFloatValue plus modelY modelSpeed

                speedValue =
                    viewIf (viewInfix viewBoolValue lteq modelY (viewFloat 0))
                        (viewFloat 4)
                        (viewInfix viewFloatValue minus modelSpeed (viewFloat 0.2))

                body =
                    [ ( "y", yValue.view )
                    , ( "speed", speedValue.view )
                    ]
                        |> viewRecordUpdates "model"
            in
            viewSimulationStep "simulateAll" body model -- TODO: threw out simulateAll -- (simulateAll model)

        -- viewPhysMove =
        --     let
        --         fieldValue =
        --             viewInfix viewFloatValue plus modelY modelSpeed
        --     in
        --     viewSimulationStep "physMove" (viewRecordUpdate "y" fieldValue.view) (physMove model)

        -- viewGravity =
        --     let
        --         fieldValue =
        --             viewInfix viewFloatValue minus modelSpeed (viewFloat 0.2)
        --     in
        --     viewSimulationStep "gravity" (viewRecordUpdate "speed" fieldValue.view) (gravity (physMove model))
    in
    Html.div [ Attr.style "font-family" "monospace" ]
        [ viewSimulationAll
        -- , viewPhysMove
        -- , viewGravity
        ]








------------------------------------------
-- Utility Functions
------------------------------------------


-- mixture of initialize and iterate
initerate : Int -> (a -> a) -> a -> List a
initerate n f a =
    if n > 0 then
        a :: initerate (n - 1) f (f a)
    else
        []


distSquared (x1, y1) (x2, y2) =
    (x1 - x2)^2 + (y1 - y2)^2


scaleVec factor (x, y) =
    (factor * x, factor * y)


directionToVector : Float -> Float -> (Float, Float)
directionToVector angle length =
    ( sin (degrees angle) * length
    , cos (degrees angle) * length
    )


fromFloat num =
    String.fromFloat (roundOn 2 num)


roundOn : Float -> Float -> Float
roundOn digits n =
    toFloat (round (n * 10 ^ digits)) / 10 ^ digits


keyDecoder : Decode.Decoder String
keyDecoder =
    Decode.field "key" Decode.string


offsetDecoder : Decode.Decoder (Float, Float)
offsetDecoder =
    Decode.map2 Tuple.pair
        (Decode.field "offsetX" Decode.float)
        (Decode.field "offsetY" Decode.float)