customElements.define(
  "elm-canvas",
  class extends HTMLElement {
    constructor() {
      super();
      this.commands = [];
      this.mounted = false;
      this.images = {};
      this.devicePixelRatio = 1;

      this.canvasSize = { w: 0, h: 0 };

      this.evalValue = this.evalValue.bind(this)
    }

    set cmds(values) {
      this.commands = values;
      this.render();
    }

    set size(s) {
      if (s.w !== this.canvasSize.w && s.h !== this.canvasSize.h) {
        this.canvasSize = s
        if (this.mounted) {
          console.log(s)
          requestAnimationFrame(() => {
            this.canvas.style.width = this.canvasSize.w + "px";
            this.canvas.style.height = this.canvasSize.h + "px";
            this.canvas.width = this.canvasSize.w * this.devicePixelRatio;
            this.canvas.height = this.canvasSize.h * this.devicePixelRatio;
          });
        }
      }
    }

    connectedCallback() {
      requestAnimationFrame(() => {
        this.canvas = this.querySelector("canvas");
        this.context = this.canvas.getContext("2d");
        this.mounted = true;

        this.devicePixelRatio = window.devicePixelRatio || 1;
        this.canvas.style.width = this.canvasSize.w + "px";
        this.canvas.style.height = this.canvasSize.h + "px";
        this.canvas.width = this.canvasSize.w * this.devicePixelRatio;
        this.canvas.height = this.canvasSize.h * this.devicePixelRatio;

        this.render();
      });
    }

    render() {
      if (!this.mounted) return;
      // Iterate over the commands in reverse order as that's how the Elm side
      // builds them as with the linked lists
      this.context.save()
      this.context.scale(this.devicePixelRatio, this.devicePixelRatio);
      for (let i = this.commands.length - 1; i >= 0; i--) {
        this.execCommand(this.commands[i]);
      }
      this.context.restore()
      this.commands = [];
    }

    getImage(imageSrc) {
      var image = this.images[imageSrc]
      if (image == null) {
        image = new Image();
        image.src = imageSrc;
        this.images[imageSrc] = image;
      }
      return image
    }

    evalValue(value) {
      if (typeof value === 'object') {
        if (value.type === 'call') {
          return this.context[value.name](...value.args.map(this.evalValue));
        } else if (value.type === 'getImage') {
          return this.getImage(value.imageSrc);
        }
      } else {
        return value;
      }
    }

    execCommand(cmd) {
      if (cmd.type === "function") {
        this.context[cmd.name](...cmd.args);
      } else if (cmd.type === "field") {
        this.context[cmd.name] = cmd.value;
      } else if (cmd.type === "drawImage") {
        var image = this.getImage(cmd.imageSrc);
        if (!image.complete) {
          this.context.fillStyle = "black";
          this.context.fillRect(...cmd.args);
        } else {
          this.context.drawImage(image, ...cmd.args)
        }
      } else if (cmd.type === "fillPattern") {
        var image = this.getImage(cmd.imageSrc);
        if (!image.complete) {
          this.context.fillStyle = "black";
        } else {
          this.context.fillStyle = this.context.createPattern(image, ...cmd.args);
        }
      }

      // New command executor
      else if (cmd.type === 'call') {
        this.context[cmd.name](...cmd.args.map(this.evalValue))
      } else if (cmd.type === '=') {
        this.context[cmd.name] = this.evalValue(cmd.value);
      } else if (cmd.type === '+=') {
        this.context[cmd.name] += this.evalValue(cmd.value);
      } else if (cmd.type === '*=') {
        this.context[cmd.name] *= this.evalValue(cmd.value);
      } else if (cmd.type === '-=') {
        this.context[cmd.name] -= this.evalValue(cmd.value);
      } else if (cmd.type === '/=') {
        this.context[cmd.name] /= this.evalValue(cmd.value);
      }
    }
  }
);
